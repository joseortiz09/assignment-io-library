%define SYS_EXIT 60
%define SYS_WRITE 1
%define SYS_READ 0
%define STDOUT 1
%define SYMBOL_TAB 0x9
%define SYMBOL_NEWLINE 0xA
%define SYMBOL_SPACE 0x20
%define ASCII_NINE 0x39
%define ASCII_ZERO 0x30
%define DECIMAL_DIVIDER 0xA



section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYS_EXIT ; Recording the system call number
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, 0
    .loop:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rax, SYS_WRITE ; system write
    mov rdi, STDOUT ; stdout
    syscall
    ret
; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, SYS_WRITE ; system write
    mov rdx, 1
    mov rdi, STDOUT
    syscall
    add rsp, 8
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov r9, 10
    mov rax, rdi
    mov r10, rsp
    dec rsp
    .loop:
        xor rdx, rdx
        div r9
        mov rdi,rdx
        add rdi, '0'
        dec rsp
        mov byte [rsp], dil
        cmp rax, 0
        je .print
        jmp .loop
    .print:
        mov rdi, rsp
        call print_string
        mov rsp, r10
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jge .no_sign_print
    neg rdi
    mov r8, rdi
    mov rdi, '-'
    call print_char
    mov rdi, r8
    call print_uint
    ret
    .no_sign_print:
        jmp print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov al, byte [rdi]
    cmp al, byte [rsi]
    jne .not_equal
    inc rdi
    inc rsi
    test al, al
    jnz string_equals
    mov rax, 1
    ret
    .not_equal:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, SYS_READ
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rsi-0x1
    push rdi
    xor r8, r8
    xor r9, r9
    .loop:
        call read_char
        mov rdi, [rsp]
        mov rsi, [rsp+1]
        cmp rax, SYMBOL_TAB
        je .check
        cmp rax, SYMBOL_NEWLINE
        je .check
        cmp rax, SYMBOL_SPACE
        je .check
        mov r9, 1
        cmp rax, 0
        je .check
        cmp r8, rsi
        je .err
        mov [rdi+r8], al
        inc r8
        jmp .loop
    .check:
        test r9, r9
        je .loop
        mov [rdi+r8], byte 0
        mov rax, rdi
        mov rdx, r8
        jmp .end
    .err:
        xor rax, rax
    .end:
        pop rdi
        pop rsi
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor r8, r8
    xor r9, r9
    mov r10, DECIMAL_DIVIDER
    .loop:
        mov r9b, [rdi+r8]
        cmp r9b, ASCII_ZERO
        jb .end
        cmp r9b, ASCII_NINE
        ja .end
        sub r9b, ASCII_ZERO
        mul r10
        add rax, r9
        inc r8
        jmp .loop
    .end:
        mov rdx, r8
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
	cmp byte [rdi], '-'
	je .is_negative
	call parse_uint
	ret
	.is_negative:
		inc rdi
		call parse_uint
		neg rax
		test rdx, rdx
		jz .end
		inc rdx
    .end:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
    cmp rax, rdx
    jge .overflow
    xor rcx, rcx
    .loop:
        mov r9b, byte[rdi+rcx]
        mov byte[rsi+rcx], r9b
        cmp rcx, rax
        je .end
        inc rcx
        jmp .loop
    .overflow:
        xor rax, rax
        ret
    .end:
        ret
